package command

// GENERATED. DO NOT EDIT.

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/go-duo/hn/bud/env"
	"github.com/go-duo/hn/bud/web"

	"golang.org/x/sync/errgroup"

	"gitlab.com/mnm/bud-lambda/command/deploy"
	cli "gitlab.com/mnm/bud/cli_old"
	"gitlab.com/mnm/bud/gen"
	"gitlab.com/mnm/bud/go/mod"
	"gitlab.com/mnm/bud/log/console"
	plugin "gitlab.com/mnm/bud/plugin_old"
)

func Run(args ...string) int {
	ctx := context.Background()
	ctx, cancel := cli.Trap(ctx, os.Interrupt)
	defer cancel()
	if err := runner(ctx, args...); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			return 0
		}
		console.Error("bud > %s", err.Error())
		return 1
	}
	return 0
}

func runner(ctx context.Context, args ...string) error {
	cli := flag.NewFlagSet("bud", flag.ContinueOnError)
	cli.SetOutput(ioutil.Discard)
	cli.Usage = func() { fmt.Println("bud help") }
	// Parse args
	if err := cli.Parse(args); err != nil {
		return err
	}
	// // Load the environment
	// env, err := env.Load()
	// if err != nil {
	// 	return err
	// }
	// fmt.Println("got env", env)
	switch arg := cli.Arg(0); arg {
	case "":
		return run(ctx, nextArgs(cli.Args())...)
	case "serve":
		return runServe(ctx, nextArgs(cli.Args())...)
	case "deploy":
		return runDeploy(ctx, nextArgs(cli.Args())...)
	default:
		return unknownArgument(arg)
	}
}

func nextArgs(args []string) []string {
	if len(args) <= 1 {
		return []string{}
	}
	return args[1:]
}

func unknownArgument(arg string) error {
	return fmt.Errorf("unknown argument %q", arg)
}

func run(ctx context.Context, args ...string) error {
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error { return runServe(ctx, args...) })
	return eg.Wait()
}

// TODO: move command to the runtime
func runServe(ctx context.Context, args ...string) error {
	cli := flag.NewFlagSet("serve", flag.ContinueOnError)
	cli.SetOutput(ioutil.Discard)
	var addr string
	cli.StringVar(&addr, "address", ":3000", "address")
	cli.Usage = func() { fmt.Println("bud help") }
	// Parse args
	if err := cli.Parse(args); err != nil {
		return err
	}
	modfile, err := mod.Find(".")
	if err != nil {
		return err
	}
	genfs := gen.New(os.DirFS(modfile.Directory()))
	genfs.Add(map[string]gen.Generator{
		"bud/plugin": gen.DirGenerator(&plugin.Generator{Modfile: modfile}),
	})
	handler, err := web.Load(genfs, modfile)
	if err != nil {
		return err
	}
	// TODO: improve this implementation
	server := &http.Server{
		Addr:    addr,
		Handler: handler,
	}
	go func() {
		<-ctx.Done()
		// TODO: this can hang forever
		server.Shutdown(context.Background())
	}()
	console.Info("Listening on http://localhost%s", addr)
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		return err
	}
	return nil
}

// TODO: figure out how to make this di compatible
func runDeploy(ctx context.Context, args ...string) error {
	modfile, err := mod.Find(".")
	if err != nil {
		return err
	}
	env, err := env.LoadBudLambdaEnv()
	if err != nil {
		return err
	}
	command := deploy.New(env, modfile)
	return command.Run(ctx)
}
