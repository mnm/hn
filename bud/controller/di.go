package controller

// GENERATED. DO NOT EDIT.

// di -d="github.com/go-duo/hn/bud/controller.Controller" -o ./bud/controller
// Note: You'll need to comment out Load each time

import (
	"github.com/go-duo/hn/bud/view"
	controller "github.com/go-duo/hn/controller"
	"github.com/go-duo/hn/internal/hn"
)

func Load(viewServer *view.Server) *Controller {
	hnClient := hn.New()
	controllerController := &controller.Controller{HN: hnClient}
	return PrivateLoad(controllerController, viewServer)
}
