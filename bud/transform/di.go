package transform

// GENERATED. DO NOT EDIT.

import (
	"gitlab.com/mnm/bud-tailwind/transform/tailwind"
	"gitlab.com/mnm/bud/go/mod"
	v8 "gitlab.com/mnm/bud/js/v8"
	"gitlab.com/mnm/bud/svelte"
)

func Load(modfile mod.File) (*Transformer, error) {
	tailwindTransform := tailwind.New(modfile)
	v8 := v8.New()
	// TODO: "Dev: true" can't be generated from di
	svelteInput := &svelte.Input{VM: v8, Dev: true}
	svelteCompiler := svelte.New(svelteInput)
	svelteTransformable := svelte.NewTransformable(svelteCompiler)
	return PrivateLoad(svelteTransformable, tailwindTransform)
}
