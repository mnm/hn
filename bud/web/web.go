package web

// GENERATED. DO NOT EDIT.

import (
	"net/http"

	"gitlab.com/mnm/bud/hot"
	"gitlab.com/mnm/bud/middleware"
	"gitlab.com/mnm/bud/router"
	"github.com/go-duo/hn/bud/controller"
	"github.com/go-duo/hn/bud/public"
	"github.com/go-duo/hn/bud/view"
)

func PrivateLoad(
	controller *controller.Controller,
	hot *hot.Server,
	public public.Middleware,
	router *router.Router,
	view *view.Server,
) (*Server, error) {
	// Routing
	router.Get("/", http.HandlerFunc(controller.Index))
	router.Get("/:id", http.HandlerFunc(controller.Show))
	router.Get("/new", view.Handler("/new"))
	// Compose the middleware together
	middleware := middleware.Compose(
		public,
		hot,
		view,
		router,
	)
	handler := middleware.Middleware(http.NotFoundHandler())
	return &Server{handler}, nil
}

type Server struct {
	http.Handler
}
