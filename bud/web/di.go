package web

// GENERATED. DO NOT EDIT.

// di -d="github.com/go-duo/hn/bud/web.Server" -o ./bud/web
// Note: You'll need to comment out Load each time

import (
	controller1 "github.com/go-duo/hn/bud/controller"
	"github.com/go-duo/hn/bud/public"
	"github.com/go-duo/hn/bud/transform"
	"github.com/go-duo/hn/bud/view"
	"gitlab.com/mnm/bud/gen"
	"gitlab.com/mnm/bud/go/mod"
	"gitlab.com/mnm/bud/hot"
	v8 "gitlab.com/mnm/bud/js/v8"
	router "gitlab.com/mnm/bud/router"
)

func Load(genfs gen.FS, modfile mod.File) (*Server, error) {
	routerRouter := router.New()
	v8 := v8.New()
	transformer, err := transform.Load(modfile)
	if err != nil {
		return nil, err
	}
	viewServer := view.New(genfs, modfile, v8, transformer)
	publicMiddleware := public.New(genfs)
	hotServer := hot.New(genfs)
	controller1Controller := controller1.Load(viewServer)
	webServer, err := PrivateLoad(controller1Controller, hotServer, publicMiddleware, routerRouter, viewServer)
	if err != nil {
		return nil, err
	}
	return webServer, err
}
