package env

import (
	bud_lambda_env "gitlab.com/mnm/bud-lambda/env"
	budenv "gitlab.com/mnm/bud/env"
)

// func Load() (env *Env, err error) {
// 	c := budenv.Collect()
// 	env = new(Env)
// 	env.AWS = new(AWS)
// 	env.AWS.AccessKey = c.String("AWS_ACCESS_KEY_ID")
// 	env.AWS.SecretKey = c.String("AWS_SECRET_ACCESS_KEY")
// 	env.AWS.Region = c.StringOr("AWS_REGION", "us-east-2")
// 	if err := c.Error(); err != nil {
// 		return nil, err
// 	}
// 	return env, nil
// }

// type Env struct {
// 	AWS *AWS
// }

type AWS struct {
	bud_lambda_env.AWS
}

func LoadBudLambdaEnv() (*bud_lambda_env.Env, error) {
	c := budenv.Collect()
	env := new(bud_lambda_env.Env)
	env.AWS = new(bud_lambda_env.AWS)
	env.AWS.AccessKey = c.String("AWS_ACCESS_KEY_ID")
	env.AWS.SecretKey = c.String("AWS_SECRET_ACCESS_KEY")
	env.AWS.Region = c.StringOr("AWS_REGION", "us-east-2")
	if err := c.Error(); err != nil {
		return nil, err
	}
	return env, nil
}
