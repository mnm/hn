package web_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/mnm/bud/gen"
	"gitlab.com/mnm/bud/go/mod"
	"github.com/go-duo/hn/bud/web"
	"github.com/matryer/is"
)

func TestShow(t *testing.T) {
	is := is.New(t)
	modfile, err := mod.Find(".")
	is.NoErr(err)
	bf := bfs.New(os.DirFS(modfile.Directory()))
	server, err := web.Load(bf, modfile)
	is.NoErr(err)
	req := httptest.NewRequest("GET", "/1", nil)
	rec := httptest.NewRecorder()
	server.ServeHTTP(rec, req)
	res := rec.Result()
	is.Equal(res.StatusCode, http.StatusOK)
	body, err := ioutil.ReadAll(res.Body)
	is.NoErr(err) // unable to read the body
	sb := string(body)
	fmt.Println(sb)
	is.True(strings.Contains(sb, "Hacker News"))
	is.True(strings.Contains(sb, "Y Combinator"))
	is.True(strings.Contains(sb, "ycombinator.com"))
	is.True(strings.Contains(sb, "pg"))
	is.True(strings.Contains(sb, "Is there anywhere to eat on Sandhill Road?"))
}
