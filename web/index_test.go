package web_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/mnm/bud/gen"
	"gitlab.com/mnm/bud/go/mod"
	"github.com/go-duo/hn/bud/web"
	"github.com/matryer/is"
)

func TestIndex(t *testing.T) {
	is := is.New(t)
	modfile, err := mod.Find(".")
	is.NoErr(err)
	bf := bfs.New(os.DirFS(modfile.Directory()))
	server, err := web.Load(bf, modfile)
	is.NoErr(err)
	req := httptest.NewRequest("GET", "/", nil)
	rec := httptest.NewRecorder()
	server.ServeHTTP(rec, req)
	res := rec.Result()
	is.Equal(res.StatusCode, http.StatusOK)
	body, err := ioutil.ReadAll(res.Body)
	is.NoErr(err) // unable to read the body
	sb := string(body)
	is.True(strings.Contains(sb, "Hacker News"))
	is.True(strings.Contains(sb, "New"))
	is.True(strings.Contains(sb, "Ask"))
	is.True(strings.Contains(sb, "Show"))
	is.True(strings.Contains(sb, "1."))
	is.True(strings.Contains(sb, "30."))
}
