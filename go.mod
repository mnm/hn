module github.com/go-duo/hn

go 1.17

require (
	github.com/matryer/is v1.4.0
	gitlab.com/mnm/bud v0.0.0-20211017185247-da18ff96a31f
	gitlab.com/mnm/bud-lambda v0.0.0-00010101000000-000000000000
	gitlab.com/mnm/bud-tailwind v0.0.0-00010101000000-000000000000
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)

require (
	github.com/armon/go-radix v1.0.0 // indirect
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/evanw/esbuild v0.12.24 // indirect
	github.com/gedex/inflector v0.0.0-20170307190818-16278e9db813 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/jackc/puddle v1.1.4 // indirect
	github.com/matthewmueller/text v0.0.0-20210424201111-ec1e4af8dfe8 // indirect
	go.kuoruan.net/v8go-polyfills v0.4.0 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20210908233432-aa78b53d3365 // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
	rogchap.com/v8go v0.6.0 // indirect
)

replace gitlab.com/mnm/bud => ../bud

replace gitlab.com/mnm/bud-tailwind => ../bud-tailwind

replace gitlab.com/mnm/bud-lambda => ../bud-lambda
