package view

import (
	"os"
	"strings"
	"testing"

	"github.com/go-duo/hn/internal/hn"

	"gitlab.com/mnm/bud/gen"
	"gitlab.com/mnm/bud/go/mod"
	v8 "gitlab.com/mnm/bud/js/v8"
	"github.com/go-duo/hn/bud/view"
	"github.com/matryer/is"
)

func TestRun(t *testing.T) {
	is := is.New(t)
	modfile, err := mod.Find(".")
	is.NoErr(err)
	bf := bfs.New(os.DirFS(modfile.Directory()))
	v8 := v8.New()
	vs := view.New(bf, modfile, v8)
	res, err := vs.Render("/:id", view.Map{
		"story": &hn.Story{
			Children: []hn.Children{},
		},
	})
	is.NoErr(err)
	is.Equal(res.Status, 200)
	is.True(strings.Contains(res.Body, "Hacker News"))
}
