package controller_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/mnm/bud/gen"
	"gitlab.com/mnm/bud/go/mod"
	v8 "gitlab.com/mnm/bud/js/v8"
	"github.com/go-duo/hn/bud/view"

	"gitlab.com/mnm/bud/router"
	"github.com/go-duo/hn/bud/controller"
	"github.com/matryer/is"
)

func TestIndex(t *testing.T) {
	is := is.New(t)
	router := router.New()
	modfile, err := mod.Find(".")
	is.NoErr(err)
	bf := bfs.New(os.DirFS(modfile.Directory()))
	vm := v8.New()
	view := view.New(bf, modfile, vm)
	controller, err := controller.Load(router, view)
	is.NoErr(err)
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	controller.Index(w, r)
	res := w.Result()
	is.Equal(res.StatusCode, http.StatusOK)
	body, err := ioutil.ReadAll(res.Body)
	is.NoErr(err) // unable to read the body
	sb := string(body)
	fmt.Println(sb)
}
